<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace Console;

use Model\CoreSettings;
use phpOMS\ApplicationAbstract;
use phpOMS\Console\CommandManager;
use phpOMS\DataStorage\Cache\Pool as CachePool;
use phpOMS\DataStorage\Database\Pool;
use phpOMS\DataStorage\Session\HttpSession;
use phpOMS\Dispatcher\Dispatcher;
use phpOMS\Event\EventManager;
use phpOMS\Localization\L11nManager;
use phpOMS\Module\ModuleManager;
use phpOMS\Router\Router;

/**
 * Controller class.
 *
 * @category   Framework
 * @package    Framework
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
class ConsoleApplication extends ApplicationAbstract
{

    /**
     * Constructor.
     *
     * @param array $config Core config
     * @param array $arg    Call argument
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function __construct(array $config, array $arg)
    {
        $this->dbPool = new Pool();
        $this->dbPool->create('core', $config['db']);

        /* TODO: implement persistent file session with time limit (maybe partially the same as socket session)  */
        $this->cachePool      = new CachePool($this->dbPool);
        $this->appSettings    = new CoreSettings($this->dbPool->get());
        $this->eventManager   = new EventManager();
        $this->router         = new Router();
        $this->sessionManager = new HttpSession(36000);
        $this->moduleManager  = new ModuleManager($this);
        $this->l11nManager    = new L11nManager();
        $this->dispatcher     = new Dispatcher($this);
        $commandManager       = new CommandManager();

        $modules = $this->moduleManager->getActiveModules();
        $this->moduleManager->initModule($modules);

        $commandManager->attach('', function ($para) {
            echo 'Use -h for help.';
        }, null);

        $commandManager->trigger($arg[1] ?? '', $arg);
    }
}
